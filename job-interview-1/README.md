# Job Interview 1

## Outcome
- **Mampu menjelaskan dan mengimplementasikan dasar OOP**
- Mempu menganalisis permasalahan dunia nyata dan menerjemahkannya dalam solusi berbasis OOP

## Deskripsi
1. Pilih produk aplikasi / game digital yang Kamu suka (IG / Tiktok / BCA Mobile / Gojek / Grab / Mobile Legend / Photoshop / Office / dsb.)
2. Buat list use case prioritas dari produk digital tersebut (tabel MD)
3. Desain dan implementasikan dalam bentuk program sederhana berbasis Object Oriented Programming dengan ketentuan
    - Buat class diagram sederhananya  
    - Mengimplementasikan 4 pilar OOP dengan tepat
        1. Encapsulation
        2. Abstraction
        3. Inheritance
        4. Polymorphism
            - Method overriding
            - Method overloading
    - Mengimplementasikan keseluruhan fitur OOP
    - Diberi komentar untuk tiap class / method [tips](https://stackoverflow.blog/2021/12/23/best-practices-for-writing-code-comments/)
4. Beri interface minimal dalam bentuk Command Line interface
5. Buat video presentasi dari produk yang dibuat (audience publik)
6. Mampu menjelaskan OOP dari pertemuan 1 - 7

## Aspek Penilaian
- Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat
- Mampu menjelaskan algoritma dari solusi yang dibuat
- Mampu menjelaskan konsep dasar OOP
- Mampu mendemonstrasikan penggunaan Encapsulation secara tepat
- Mampu mendemonstrasikan penggunaan Abstraction secara tepat
- Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat
- Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat
- Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table
- Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video
- Inovasi UX


## Contoh
```java

/*
Instagram
1. Bisa posting Konten
2. Bisa respon Konten: like, comment, bookmark, share
3. Bisa kirim pesan personal
4. Bisa follow

InstagramApp
- contents: Content[]
    - id
    - media: Media[]
    - responses: Response[]
- users: User[]
    - id
    - username
    - displayName
    - photoURL
    - followings: User[]
    - followers: User[]
    - contents: Content[]
    - post(Content content)
    - respond(Response response)
*/

import java.util.ArrayList;

class User {
    private String username;
    public User(String username) {
        this.username = username;
    }
    
    public String getUsername() {
        return this.username;
    }
}

class InstagramApp {
    private String version = "0.0.1";
    public void showVersion() {
        System.out.println("Version " + this.version);
    }
    
    public void createUser(User newUser) {
        this.users.add(newUser);
        System.out.println("User " + newUser.getUsername() + " berhasil ditambahkan, jumlah user Instagram saat ini ada " + this.users.size());
    }
    
    private ArrayList<User> users = new ArrayList<User>(); 
}

public class Main
{
    public static void main(String[] args) {
        InstagramApp app = new InstagramApp();
        app.showVersion();
        
        User user1 = new User("syamil_mr");
        app.createUser(user1);
        
        User user2 = new User("the_dian");
        app.createUser(user2);
    }
}
```
